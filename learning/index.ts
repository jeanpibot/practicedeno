// programación dinamica

// Fibonacci
const linearFibo = (n: number) : number => {
    let memo = [0, 1];
    for (let actual = 2; actual <= n; actual++) {
        memo[actual] = memo[actual - 2] + memo[actual -1];
        console.log(memo);
    }
    return memo[n];
}

console.log(linearFibo(5));
